#include <stdio.h>
int main ()
{
    int number;
    printf("Enter your number : ");
    scanf("%d", &number);

    if(number==0)
    {
       printf("This number is zero.");
    }
    else if(number>0)
    {
        printf("This number is positive.");
    }
    else
    {
        printf("This number is negative.");
    }
    return 0;
}
