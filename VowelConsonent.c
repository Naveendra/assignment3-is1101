#include <stdio.h>
int main()
{
    char l;
    printf("Type a character from the alphabet : ");
    scanf("%c", &l);

    if(l=='a' || l=='e' || l=='i' || l=='o' || l=='u' ||
       l=='A' || l=='E' || l=='I' || l=='O' || l=='U')
    {
        printf("This is a vowel.");
    }

    else if (l=='b' || l=='B' || l=='c' || l=='C' || l=='d' || l=='D' ||
             l=='f' || l=='F' || l=='g' || l=='G' || l=='h' || l=='H' ||
             l=='j' || l=='J' || l=='k' || l=='K' || l=='l' || l=='L' ||
             l=='m' || l=='M' || l=='n' || l=='N' || l=='p' || l=='P' ||
             l=='q' || l=='Q' || l=='r' || l=='R' || l=='s' || l=='S' ||
             l=='t' || l=='T' || l=='v' || l=='V' || l=='w' || l=='W' ||
             l=='x' || l=='X' || l=='y' || l=='Y' || l=='z' || l=='Z')
    {
        printf("This is a consonent");
    }

        else
        {
            printf("You have entered a wrong character, please try again!!!");
        }

    return 0;
}
